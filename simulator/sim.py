import numpy as np
from tqdm import tqdm
from simulator.funcs import angleFuncs
import matplotlib.pyplot as plt

class sim:

    def __init__(self, model, EOM, controller, actuator, showAnimation = True, animator = None, integrator = 'Euler', intergratorKwargs = {}, angleIndices = [0, 1, 2]):
        self.simVars = {}
        self.model = model
        self.EOM = EOM
        self.controller = controller
        self.actuator = actuator
        self.integrator_properties = {'kwargs':intergratorKwargs}
        knownIntegrators = {'Euler':self._integrator_Euler}
        if integrator not in knownIntegrators:
            raise ValueError('Unknown integrator scheme "{}"'.format(integrator))
        else:
            self.integrator = knownIntegrators[integrator]

        self.angleIndices = angleIndices
        
        # Wrap-able functions, outside of the simulation, we assign 
        # different functions to these to do extra steps before/after these blocks
        self.doControl = self.controller.control
        self.doActuation = self.actuator.actuate
        self.doForcesMoments = self.model.getForcesMoments
        self.doEOM = self.EOM

        self.animate = self._dummyAnimator
        self.showAnimation = showAnimation
        self.animator = animator
        if animator is not None:
            # self.animationUpdateRateFactor = 8 # ~30 FPS for saved animation
            self.animationUpdateRateFactor = 4 # ~60 FPS for saved animation
        return None

    def _integrator_Euler(self, simVars):
        step = simVars['currentTimeStep_index']
        x = simVars['state'][step]
        # NOTE: we take step + 1 for state derivative since it is updated from EOM (i.e. latest available)
        x_dot = simVars['stateDerivative'][step+1]
        dt = simVars['dt']
        return x + x_dot*dt

    def assignIntegrator(self, integrator):
        self.integrator = integrator

    def setInitial(self, time, state, inputs, reference, **kwargs):
        self.time = time
        self.T = time[-1]
        self.dt = 0
        if len(self.time) != len(state) or len(self.time) != len(inputs) or len(self.time) != len(reference):
            raise ValueError('Axis mismatch; either the state, inputs, or reference vector does not match the time array')
        self.state = state
        self.inputs = inputs
        self.reference = reference
        self.currentTimeStep_index = 0
        self.stateDerivative = np.zeros(self.state.shape)
        if len(self.angleIndices) > 0:
            self.quat = np.zeros((len(time), 1, 4))
            self.quat[0, :, :] = angleFuncs.Eul2Quat(self.state[0, :, self.angleIndices])
            self.quatDot = np.zeros((len(time), 1, 4))
        else:
            self.quat = None
            self.quatDot = None
        self.forces = np.zeros((len(time), 1, 3))
        self.moments = np.zeros((len(time), 1, 3))
        self.inputs_CMD = self.inputs.copy()
        self._assignSimVars(**kwargs)
        if self.animator is not None and self.showAnimation:
            self.addSimVar('AnimationUpdateFactor', self.animationUpdateRateFactor)
            self.animate = self.animator.update
            self.animator._initActorsDraw({self.model.modelID:self.simVars})

    def _assignSimVars(self, **kwargs):
        # Add essential
        self.addSimVar('time', self.time)
        self.addSimVar('T', self.T)
        self.addSimVar('dt', self.dt)
        self.addSimVar('state', self.state)
        self.addSimVar('state_noisy', self.state)
        self.addSimVar('quat', self.quat)
        self.addSimVar('quatDot', self.quatDot)
        self.addSimVar('inputs', self.inputs)
        self.addSimVar('inputs_CMD', self.inputs_CMD)
        self.addSimVar('reference', self.reference)
        self.addSimVar('currentTimeStep_index', self.currentTimeStep_index)
        self.addSimVar('stateDerivative', self.stateDerivative)
        self.addSimVar('stateDerivative_noisy', self.stateDerivative)
        self.addSimVar('forces', self.forces)
        self.addSimVar('moments', self.moments)
        self.addSimVar('model', self.model)
        self.addSimVar('controller', self.controller)
        self.addSimVar('EOM', self.EOM)
        self.addSimVar('actuator', self.actuator)
        self.addSimVar('intergrator', self.integrator)
        self.addSimVar('intergrator_properties', self.integrator_properties)
        # Add any additional arguments
        if len(kwargs):
            for k in kwargs.keys():
                self.addSimVar(k, kwargs[k])

    def addSimVar(self, name, variable):
        # Check for conflicts
        if name in self.simVars.keys():
            raise ValueError(f'{name} already exists in simulation variables!')
        else:
            self.simVars.update({name:variable})

    def updateSimVar(self, name, variable):
        self.simVars.update({name:variable})

    def _dummyAnimator(self, *args):
        return None

    def run(self):
        try:
            for i in tqdm(range(len(self.time) - 1), leave = False):
                self.currentTimeStep_index = i
                # Need to update index in simVars since we reassign it here
                self.updateSimVar('currentTimeStep_index', self.currentTimeStep_index)
                # Update timestep, in case it is variable
                self.dt = self.time[self.currentTimeStep_index+1] - self.time[self.currentTimeStep_index]
                # Need to update dt in simVars since we reassign it here
                self.updateSimVar('dt', self.dt)
                # Call controller
                self.inputs_CMD[self.currentTimeStep_index + 1] = self.doControl(self.simVars)
                # Convert commanded inputs into true inputs through actuator
                self.inputs[self.currentTimeStep_index + 1] = self.doActuation(self.simVars)
                # Use models to get forces and moments from current state and inputs
                self.forces[self.currentTimeStep_index + 1], self.moments[self.currentTimeStep_index + 1] = self.doForcesMoments(self.simVars)
                # Use equations of motion to obtain state derivative & move quadrotor
                # self.stateDerivative[self.currentTimeStep_index + 1] = self.EOM(self.simVars)
                self.stateDerivative[self.currentTimeStep_index + 1] = self.doEOM(self.simVars)
                # Update state using integrator
                self.state[self.currentTimeStep_index + 1] = self.integrator(self.simVars)
                # Animate, if animator is passed
                if self.animator is not None:
                    self.animate(i, {self.model.modelID:self.simVars})
        except Exception as e:
            print(f'[ ERROR ] Simulation crashed: {e}')
            # Truncate data to before crash
            self.updateSimVar('time', self.simVars['time'][:i])
            self.updateSimVar('state', self.simVars['state'][:i])
            self.updateSimVar('quat', self.simVars['quat'][:i])
            self.updateSimVar('state_noisy', self.simVars['state_noisy'][:i])
            self.updateSimVar('quatDot', self.simVars['quatDot'][:i])
            self.updateSimVar('inputs', self.simVars['inputs'][:i])
            self.updateSimVar('inputs_CMD', self.simVars['inputs_CMD'][:i])
            self.updateSimVar('reference', self.simVars['reference'][:i])
            self.updateSimVar('stateDerivative', self.simVars['stateDerivative'][:i])
            self.updateSimVar('stateDerivative_noisy', self.simVars['stateDerivative_noisy'][:i])
            self.updateSimVar('forces', self.simVars['forces'][:i])
            self.updateSimVar('moments', self.simVars['moments'][:i])
        if self.animator is not None:
            if plt.fignum_exists(self.animator.fig):
                plt.close(self.animator.fig)
            ani = self.animator.posteriorAnimation({self.model.modelID:self.simVars})
            aniSaver = self.animator.saveAnimation
            self.addSimVar('ani', ani)
            self.addSimVar('aniSaver', aniSaver)