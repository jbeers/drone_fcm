'''
Class which imports and makes use of the identified drone model

Created by Jasper van Beers
Contact: j.j.vanbeers@tudelft.nl
Date: 09-02-2022
'''
import numpy as np
import json
from re import sub as reSub

'''
Quadrotor model class
'''
class Quadrotor:

	def __init__(self, model_file):
		# Open model file
		with open(model_file, 'r') as f:
			model_data = json.load(f)

		# Extract quadrotor parameters
		self.droneParams = model_data['droneParams']
		# Convert drone inertia from string to matrix
		self.droneParams['Iv'] = np.array(np.matrix(self.droneParams['Iv']))
		self.hasGravity = model_data['metadata']['hasGravity']
		self.modelID = model_data['metadata']['model ID']
		self.isNormalized = model_data['metadata']['isNormalized']

		# Extract aerodynamic model
		AeroModel = model_data['model']
		# Load model per axis 
		self.FxModel = DronePolynomialModel(AeroModel['Fx'], self.droneParams, hasGravity=model_data['metadata']['hasGravity'])
		self.FyModel = DronePolynomialModel(AeroModel['Fy'], self.droneParams, hasGravity=model_data['metadata']['hasGravity'])
		self.FzModel = DronePolynomialModel(AeroModel['Fz'], self.droneParams, hasGravity=model_data['metadata']['hasGravity'])
		self.MxModel = DronePolynomialModel(AeroModel['Mx'], self.droneParams, hasGravity=model_data['metadata']['hasGravity'])
		self.MyModel = DronePolynomialModel(AeroModel['My'], self.droneParams, hasGravity=model_data['metadata']['hasGravity'])
		self.MzModel = DronePolynomialModel(AeroModel['Mz'], self.droneParams, hasGravity=model_data['metadata']['hasGravity'])
		# Load offsets
		self.Fx0 = AeroModel['Fx']['Offset']
		self.Fy0 = AeroModel['Fy']['Offset']
		self.Fz0 = AeroModel['Fz']['Offset']
		self.Mx0 = AeroModel['Mx']['Offset']
		self.My0 = AeroModel['My']['Offset']
		self.Mz0 = AeroModel['Mz']['Offset']

		self.DiffSys = None
		if 'DiffSys' in model_data.keys():
			self.DiffSys = DroneDiffSystem(model_data['DiffSys'], self.droneParams, self.FxModel.droneGetModelInput)



	def getSignMask(self):
		# Define dictionary which maps the rotor numbers to their position on the quadrotor (e.g. rotor 4 is located 'front left')
		invRotorConfig = {v:k for k, v in self.droneParams['rotor config'].items()}
		# Extract ditcionary which maps the yaw sign to CW (clockwise) and CCW (counterclockwise) rotation of the rotors
		r_sign = self.droneParams['r_sign']
		# Create a toggle to parse through rotor mapping using text cues (e.g. front, left, etc.)
		signMap = {True:1, False:-1}
		# Create a matrix mask describing the signs of each rotor on each of the control moments
		# -> A matrix which describes how the rotor configuration produces rotations of the quadrotor and the sign therein
		self.signMask = np.array([[signMap[invRotorConfig[1].endswith('left')], signMap[invRotorConfig[2].endswith('left')], signMap[invRotorConfig[3].endswith('left')], signMap[invRotorConfig[4].endswith('left')]],
							[signMap[invRotorConfig[1].startswith('front')], signMap[invRotorConfig[2].startswith('front')], signMap[invRotorConfig[3].startswith('front')], signMap[invRotorConfig[4].startswith('front')]],
							r_sign[self.droneParams['rotor 1 direction']]*np.array([1, -1, -1, 1])]).T
		return self.signMask.copy()


	def getForcesMoments(self, simVars):
		step = simVars['currentTimeStep_index']
		# We use the true state here (instead of noisy) since this is where the system really is. 
		state = simVars['state'][step][:, :9]
		# NOTE: trueAction is step+1 since we just updated actuator information in sim loop. (i.e. latest actuator info)
		trueAction = simVars['inputs'][step+1]

		# Prepare inputs
		modelInputs = self.FxModel.droneGetModelInput(state, trueAction)

		if self.isNormalized:
			# Extract normalizing factors
			F_den = np.array(modelInputs['F_den']).reshape(-1)
			M_den = np.array(modelInputs['M_den']).reshape(-1)
		else:
			F_den, M_den = 1, 1		

		# Make predictions
		Fx = self.FxModel.predict(modelInputs).__array__()[0][0]*F_den - self.Fx0
		Fy = self.FyModel.predict(modelInputs).__array__()[0][0]*F_den - self.Fy0
		Fz = self.FzModel.predict(modelInputs).__array__()[0][0]*F_den - self.Fz0
		Mx = self.MxModel.predict(modelInputs).__array__()[0][0]*M_den - self.Mx0
		My = self.MyModel.predict(modelInputs).__array__()[0][0]*M_den - self.My0
		Mz = self.MzModel.predict(modelInputs).__array__()[0][0]*M_den - self.Mz0

		# Build force and moment vectors
		F = (np.array([Fx, Fy, Fz])).reshape(1, -1)
		M = (np.array([Mx, My, Mz])).reshape(1, -1)
		
		return F, M




class PolynomialModel:
	def __init__(self, model):
		# Keep track of important model metadata
		self._model = model
		self.polynomial = model['regressors']
		self.offset = model['Offset']
		self.coefficients = np.matrix(model['coefficients']).T
		self.makeRegressors()


	def predict(self, x):
		# Make predictions
		A = self._BuildRegressorMatrix(x, hasBias = ('bias' in self.polynomial))
		return A*self.coefficients


	def _BuildRegressorMatrix(self, data, hasBias = True):
		# Pre-allocate matrix
		N = len(self.regressors)
		regMat = np.matrix(np.ones((data.shape[0], N)))
		# Fill in matrix using regressors and data
		for i, reg in enumerate(self.regressors):
			regMat[:, i] = np.matrix(reg.resolve(data)).T
		# Pre-pend the bias vector, if present
		if hasBias:
			biasVec = np.matrix(np.ones((data.shape[0], 1)))
			regMat = np.hstack((biasVec, regMat))
		return regMat


	def makeRegressors(self):
		parsing = self.Parser()
		self.regressors = []
		for p in self.polynomial:
			if p != 'bias':
				p_RPN = parsing.parse(p)
				reg = self.Regressor(p_RPN)
				self.regressors.append(reg)


	# Class to convert string equations to postfix form (i.e. Reverse Polish Notation - RPN) which can be easily interpretted from left to right.
	class Parser:
		# Initialize the RPN (output) stack and operator stack (which handles order of operations prior to addition in the output stack)
		def __init__(self):
			self.sub = reSub
			self.operatorStack = []
			self.outputStack = []
			# Define allowable operators, along with their precedence and associativity
			self.operatorInfo = {
				'^':{'precedence':4,
						'associativity':'R'},
				'*':{'precedence':3,
						'associativity':'L'},
				'/':{'precedence':3,
						'associativity':'L'},
				'+':{'precedence':2,
						'associativity':'L'},
				'-':{'precedence':2,
						'associativity':'L'}                   
			}

		# Main parsing function, which converts an input string equation into RPN form.
		def parse(self, inputString):
			self.refresh()
			self.tokens = self.tokenize(inputString)
			RPN = self.shuntYard(self.tokens)
			if len(RPN) == 0:
				return [inputString]
			else:
				return RPN

		# Empty (any) previously parsed information, and reset for parsing new strings
		def refresh(self):
			self.operatorStack = []
			self.outputStack = []

		# Convert input string into tokens, sliced by the operators. 
		def tokenize(self, inputString):
			# remove spaces in string
			cleanString = self.sub(r'\s+', "", inputString)
			# Convert to list of characters to isolate operators and brackets
			chars = list(cleanString)
			# Tokens
			tokens = []
			token = ""
			while len(chars) != 0:
				char = chars.pop(0)
				if char in self.operatorInfo.keys() or char in ['(', ')']:
					if token != "":
						tokens.append(token)
					tokens.append(char)
					token = ""
				else:
					token += char
				if len(chars) == 0 and token != "":
					tokens.append(token)
			return tokens

		# Apply the Shunting-yard algorithm to convert the tokens into RPN form. 
		def shuntYard(self, tokens):
			while len(tokens) != 0:
				token = tokens.pop(0)
				# Check if token is a known operator
				if token in self.operatorInfo.keys():
					# Check operator priority
					if not len(self.operatorStack) == 0:
						sorting = True
						while sorting:
							push = False
							# Check top of operator stack for brackets
							if self.operatorStack[-1] not in ["(", ")"]:
								if self.operatorInfo[self.operatorStack[-1]]['precedence'] > self.operatorInfo[token]['precedence']:
									# top operator has greater priority
									push = True
								elif self.operatorInfo[self.operatorStack[-1]]['precedence'] == self.operatorInfo[token]['precedence']:
									if self.operatorInfo[self.operatorStack[-1]]['associativity'] == 'L':
										push = True
							sorting = push and self.operatorStack[-1] != '('
							if sorting:
								self.outputStack.append(self.operatorStack.pop())
							if len(self.operatorStack) == 0:
								sorting = False
					self.operatorStack.append(token)
				elif token == "(":
					self.operatorStack.append(token)
				elif token == ")":
					#Add operations to stack while in brackets
					while True:
						if len(self.operatorStack) == 0:
							break
						if self.operatorStack[-1] == "(":
							break
						self.outputStack.append(self.operatorStack.pop())
					if len(self.operatorStack) != 0 and self.operatorStack[-1] == "(":
						self.operatorStack.pop()
				else:
					self.outputStack.append(token)
			self.outputStack.extend(self.operatorStack[::-1])
			return self.outputStack

	# Class which handles regressor evaluations. The regressor structure is stored upon initialization for efficiency. 
	class Regressor:
		def __init__(self, regressorRPN):
			self.RPN = regressorRPN
			self.numberIndices = [i for i, v in enumerate(regressorRPN) if self.isFloat(v)]
			self.knownOperators = {'+':np.add, '-':np.subtract, '/':np.divide, '*':np.multiply, '^':np.power}
			self.operatorIndices = [i for i, v in enumerate(regressorRPN) if v in self.knownOperators.keys()]
			self.invVariableIndices = self.numberIndices + self.operatorIndices
			if len(self.invVariableIndices):
				self.variableIndices = [i for i in np.arange(0, len(regressorRPN)) if i not in self.invVariableIndices]
			else:
				self.variableIndices = np.arange(0, len(regressorRPN))

		def resolve(self, Data):
			# First convert RPN string into purely numbers
			RPN = self.RPN.copy()
			RPNStr = self.RPN.copy()
			for idx in self.variableIndices:
				RPN[idx] = Data[self.RPN[idx]]
			# Evaluate RPN expression
			stack = []
			if len(RPN) > 1:
				while len(RPN) > 0:
					token = RPN.pop(0)
					tokenStr = RPNStr.pop(0)
					if tokenStr not in self.knownOperators.keys():
						stack.append(token)
					else:
						b = np.array(stack.pop(), dtype=float)
						a = np.array(stack.pop(), dtype=float)
						stack.append(self.knownOperators[token](a, b))
				if len(stack) != 1:
					raise ValueError('There are unaccounted variables in the RPN regressor stack. Please check regressor operations are parsed correctly.')
				else:
					return stack[0]
			else:
				return np.array(RPN[0], dtype=float)

		def isFloat(self, string):
			try:
				float(string)
				return True
			except ValueError:
				return False



'''
Class which augments PolynomialModel with functions to transform and normalize the state and rotor speeds of the quadrotor for
use with the polynomial model. As such processing is system specific (e.g. for quadrotors), such processing is left out of the 
general PolynomialModel class (which assumes that the inputs are structured and processed the same as done during training).
Here, we further assume that the state has format:
State = 2-D array with columns (roll, pitch, yaw, u, v, w, p, q, r) in that order. Rows give the observed samples.
    - roll = Roll angle, in radians
    - pitch = Pitch angle, in radians
    - yaw = Yaw angle, in radians
    - u = Body linear velocity along x-axis, in m/s
    - v = Body linear velocity along y-axis, in m/s
    - w = Body linear velocity along z-axis, in m/s
    - p = (Roll rate) Body rotational velocity about x-axis, in rad/s
    - q = (Pitch rate) Body rotational velocity about y-axis, in rad/s
    - r = (Yaw rate) Body rotational velocity about z-axis, in rad/s
rotorSpeeds = 2-D array with columns (w1, w2, w3, w4) in that order. Rows give observed samples
    - wi = rotational speed of rotor i (1, 2, 3, 4) in erpm (electronic rpm, is equivalent to rpm scaled by number of rotor poles and thus depends on rotor)
''' 
class DronePolynomialModel(PolynomialModel):
	def __init__(self, model, droneParams, hasGravity = False):
		PolynomialModel.__init__(self, model)
		# Keep track of important metadata
		self.hasGravity = hasGravity
		# Define drone params
		self.droneParams = droneParams
		
		# Map induced velocity to true function or dummy function if unused by models
		self.columns = ['w1', 'w2', 'w3', 'w4', 'w2_1', 'w2_2', 'w2_3', 'w2_4', 'roll', 'pitch', 'yaw', 'u', 'v', 'w', 'p', 'q', 'r']
		self.ncolumns = ['w_tot', 'w_avg', 'w1', 'w2', 'w3', 'w4', 'w2_1', 'w2_2', 'w2_3', 'w2_4',
						'd_w1', 'd_w2', 'd_w3', 'd_w4', 'd_w_tot',
						'p', 'q', 'r', 'u_p', 'u_q', 'u_r', 'roll', 'pitch', 'yaw', 
						'U_p', 'U_q', 'U_r', '|U_p|', '|U_q|', '|U_r|',
						'u', 'v', 'w', 'mu_x', 'mu_y', 'mu_z', 'mu_vin',
						'|p|', '|q|', '|r|', '|u_p|', '|u_q|', '|u_r|',
						'|u|', '|v|', '|w|', '|mu_x|', '|mu_y|', '|mu_z|',
						'sin[roll]', 'sin[pitch]', 'sin[yaw]','cos[roll]', 'cos[pitch]', 'cos[yaw]',
						'F_den', 'M_den']
		return None

	def droneGetModelInput(self, state, rotorSpeeds):
		organizedData = self.fasterDataFrame(len(state), self.columns, np.zeros)

		# Rotor speeds
		organizedData['w1'] = rotorSpeeds[:, 0]
		organizedData['w2'] = rotorSpeeds[:, 1]
		organizedData['w3'] = rotorSpeeds[:, 2]
		organizedData['w4'] = rotorSpeeds[:, 3]

		organizedData['w2_1'] = rotorSpeeds[:, 0]**2
		organizedData['w2_2'] = rotorSpeeds[:, 1]**2
		organizedData['w2_3'] = rotorSpeeds[:, 2]**2
		organizedData['w2_4'] = rotorSpeeds[:, 3]**2

		# Attitude
		organizedData['roll'] = state[:, 0]
		organizedData['pitch'] = state[:, 1]
		organizedData['yaw'] = state[:, 2]

		# Body linear velocity
		organizedData['u'] = state[:, 3]
		organizedData['v'] = state[:, 4]
		organizedData['w'] = state[:, 5]

		# Body rotational rate
		organizedData['p'] = state[:, 6]
		organizedData['q'] = state[:, 7]
		organizedData['r'] = state[:, 8]

		# Add extra and necessary columns
		N_rot = 4 # number rotors
		R = self.droneParams['R']
		r_sign = self.droneParams['r_sign']
		rotorConfig = self.droneParams['rotor configuration']
		rotorDir = self.droneParams['rotor 1 direction']
		normalizedData = self.fasterDataFrame(organizedData.shape[0], self.ncolumns, np.zeros)

		# Pre-fill normalizedData with organizedData
		for k in self.ncolumns:
			if k in self.columns:
				normalizedData[k] = organizedData[k]

		normalizedData['w1'] = rotorSpeeds[:, 0]*2*np.pi/60
		normalizedData['w2'] = rotorSpeeds[:, 1]*2*np.pi/60
		normalizedData['w3'] = rotorSpeeds[:, 2]*2*np.pi/60
		normalizedData['w4'] = rotorSpeeds[:, 3]*2*np.pi/60

		normalizedData['d_w1'] = normalizedData['w1'] - self.droneParams['wHover (rad/s)']
		normalizedData['d_w2'] = normalizedData['w2'] - self.droneParams['wHover (rad/s)']
		normalizedData['d_w3'] = normalizedData['w3'] - self.droneParams['wHover (rad/s)']
		normalizedData['d_w4'] = normalizedData['w4'] - self.droneParams['wHover (rad/s)']

		normalizedData['w2_1'] = normalizedData['w1']**2
		normalizedData['w2_2'] = normalizedData['w2']**2
		normalizedData['w2_3'] = normalizedData['w3']**2
		normalizedData['w2_4'] = normalizedData['w4']**2

		normalizedData['F_den'] = 1
		normalizedData['M_den'] = 1

		omega = normalizedData[['w1', 'w2', 'w3', 'w4']]
		omega_tot = np.sum(omega, axis=1)
		omega2 = normalizedData[['w2_1', 'w2_2', 'w2_3', 'w2_4']]
		w_avg = np.array(self._sqrt(np.sum(omega2, axis = 1)/N_rot)).reshape(-1)
		normalizedData['w_tot'] = omega_tot
		normalizedData['d_w_tot'] = omega_tot - self.droneParams['wHover (rad/s)']*4
		normalizedData['w_avg'] = w_avg

		# Define control moments
		normalizedData['u_p'] = (omega[:, rotorConfig['front left'] - 1] + omega[:, rotorConfig['aft left'] - 1]) - (omega[:, rotorConfig['front right'] - 1] + omega[:, rotorConfig['aft right'] - 1])
		normalizedData['u_q'] = (omega[:, rotorConfig['front left'] - 1] + omega[:, rotorConfig['front right'] - 1]) - (omega[:, rotorConfig['aft left'] - 1] + omega[:, rotorConfig['aft right'] - 1])
		normalizedData['u_r'] = r_sign[rotorDir]*((omega[:, rotorConfig['front left'] - 1] + omega[:, rotorConfig['aft right']- 1]) - (omega[:, rotorConfig['front right'] - 1] + omega[:, rotorConfig['aft left'] - 1]))

		normalizedData['U_p'] = (omega2[:, rotorConfig['front left'] - 1] + omega2[:, rotorConfig['aft left'] - 1]) - (omega2[:, rotorConfig['front right'] - 1] + omega2[:, rotorConfig['aft right'] - 1])
		normalizedData['U_q'] = (omega2[:, rotorConfig['front left'] - 1] + omega2[:, rotorConfig['front right'] - 1]) - (omega2[:, rotorConfig['aft left'] - 1] + omega2[:, rotorConfig['aft right'] - 1])
		normalizedData['U_r'] = r_sign[rotorDir]*((omega2[:, rotorConfig['front left'] - 1] + omega2[:, rotorConfig['aft right']- 1]) - (omega2[:, rotorConfig['front right'] - 1] + omega2[:, rotorConfig['aft left'] - 1]))

		# Advance ratios
		normalizedData['mu_x'] = np.divide(normalizedData['u'], w_avg*R)
		normalizedData['mu_y'] = np.divide(normalizedData['v'], w_avg*R)
		normalizedData['mu_z'] = np.divide(normalizedData['w'], w_avg*R)

		# Add extra columns
		normalizedData = self._addExtraCols(normalizedData)

		return normalizedData


	def updateDroneParams(self, key, value):
		self.droneParams.update({key:value})
		return None


	def _square(self, x):
		return np.power(x, 2)


	def _sqrt(self, x):
		return np.power(x, 0.5)


	def _addExtraCols(self, Data):
		# Add abs(body velocity)
		Data['|u|'] = np.abs(Data['u'])
		Data['|v|'] = np.abs(Data['v'])
		Data['|w|'] = np.abs(Data['w'])

		# Add abs(body advance rations) 
		Data['|mu_x|'] = np.abs(Data['mu_x'])
		Data['|mu_y|'] = np.abs(Data['mu_y'])
		Data['|mu_z|'] = np.abs(Data['mu_z']) 

		# Add abs(rotational rates)
		Data['|p|'] = np.abs(Data['p'])
		Data['|q|'] = np.abs(Data['q'])
		Data['|r|'] = np.abs(Data['r'])

		# Add abs(control moments)
		Data['|u_p|'] = np.abs(Data['u_p'])
		Data['|u_q|'] = np.abs(Data['u_q'])
		Data['|u_r|'] = np.abs(Data['u_r'])
		Data['|U_p|'] = np.abs(Data['U_p'])
		Data['|U_q|'] = np.abs(Data['U_q'])
		Data['|U_r|'] = np.abs(Data['U_r'])        

		# Get trigonometric identities of attitude angles 
		Data['sin[roll]'] = np.sin(Data['roll'])
		Data['sin[pitch]'] = np.sin(Data['pitch'])
		Data['sin[yaw]'] = np.sin(Data['yaw'])

		Data['cos[roll]'] = np.cos(Data['roll'])
		Data['cos[pitch]'] = np.cos(Data['pitch'])
		Data['cos[yaw]'] = np.cos(Data['yaw'])        
		return Data


	class fasterDataFrame:
		def __init__(self, numRows, columns, npZeros):
			np.zeros = npZeros
			self.shape = (numRows, len(columns))
			self.dfvalues = npZeros(self.shape)
			self.dfmapping = {k:v for v, k in enumerate(columns)}
			self.columns = columns

		def __getitem__(self, key):
			# Check if key or list is passed
			try:
				out = self.dfvalues[:, self.dfmapping[key]]
			except TypeError:
				out = np.zeros((self.shape[0], len(key)))
				for i, k in enumerate(key):
					out[:, i] = self.dfvalues[:, self.dfmapping[k]]
			return out


		def __setitem__(self, key, newvalue):
			try:
				self.dfvalues[:, self.dfmapping[key]] = newvalue
			except TypeError:
				for i, k in enumerate(key):
					self.dfvalues[:, self.dfmapping[k]] = newvalue[:, i]



'''
DiffSys class to handle velocity form of the quadrotor model
'''
class DroneDiffSystem:

	def __init__(self, DiffSysModel, droneParams, ModelInputMapper):
		# Extract drone parameters
		self.droneParams = droneParams
		self.invIv = np.linalg.inv(droneParams['Iv'])

		# Get fixed partial derivative terms
		self.p_dot_dp = PolynomialModel(DiffSysModel['p_dot_dp'])
		self.p_dot_dq = PolynomialModel(DiffSysModel['p_dot_dq'])
		self.p_dot_dr = PolynomialModel(DiffSysModel['p_dot_dr'])

		self.q_dot_dp = PolynomialModel(DiffSysModel['q_dot_dp'])
		self.q_dot_dq = PolynomialModel(DiffSysModel['q_dot_dq'])
		self.q_dot_dr = PolynomialModel(DiffSysModel['q_dot_dr'])

		self.r_dot_dp = PolynomialModel(DiffSysModel['r_dot_dp'])
		self.r_dot_dq = PolynomialModel(DiffSysModel['r_dot_dq'])
		self.r_dot_dr = PolynomialModel(DiffSysModel['r_dot_dr'])

		# Get model-variable partial derivative terms
		self.x = []
		self.u = []
		self._DfDx_DfDu_Variable = []
		for chnl in DiffSysModel['variable']:
			dfdx_variable = chnl['dfdx']
			dfdx = {}
			for x, poly in dfdx_variable.items():
				dfdx.update({x:PolynomialModel(poly)})
				if x not in self.x:
					self.x.append(x)
			dfdu_variable = chnl['dfdu']
			dfdu = {}
			for u, poly in dfdu_variable.items():
				dfdu.update({u:PolynomialModel(poly)})
				if u not in self.u:
					self.u.append(u)
			self._DfDx_DfDu_Variable.append({'dfdx':dfdx, 'dfdu':dfdu})
		
		self._getDroneInputs = ModelInputMapper


	def getAB(self, droneInputs):
		a11 = self.invIv[0][0]
		a12 = self.invIv[0][1]
		a13 = self.invIv[0][2]

		a21 = self.invIv[1][0]
		a22 = self.invIv[1][1]
		a23 = self.invIv[1][2]
		
		a31 = self.invIv[2][0]
		a32 = self.invIv[2][1]
		a33 = self.invIv[2][2]

		Mx = self._DfDx_DfDu_Variable[0]
		My = self._DfDx_DfDu_Variable[1]
		Mz = self._DfDx_DfDu_Variable[2]

		# DfDx
		# p_ddot eqs
		A11 = (a11 * Mx['dfdx']['p'].predict(droneInputs) + self.p_dot_dp.predict(droneInputs)).__array__()[0][0]
		A12 = (a12 * My['dfdx']['q'].predict(droneInputs) + self.p_dot_dq.predict(droneInputs)).__array__()[0][0]
		A13 = (a13 * Mz['dfdx']['r'].predict(droneInputs) + self.p_dot_dr.predict(droneInputs)).__array__()[0][0]

		# q_ddot eqs 
		A21 = (a21 * Mx['dfdx']['p'].predict(droneInputs) + self.q_dot_dp.predict(droneInputs)).__array__()[0][0]
		A22 = (a22 * My['dfdx']['q'].predict(droneInputs) + self.q_dot_dq.predict(droneInputs)).__array__()[0][0]
		A23 = (a23 * Mz['dfdx']['r'].predict(droneInputs) + self.q_dot_dr.predict(droneInputs)).__array__()[0][0]

		# r_ddot eqs 
		A31 = (a31 * Mx['dfdx']['p'].predict(droneInputs) + self.r_dot_dp.predict(droneInputs)).__array__()[0][0]
		A32 = (a32 * My['dfdx']['q'].predict(droneInputs) + self.r_dot_dq.predict(droneInputs)).__array__()[0][0]
		A33 = (a33 * Mz['dfdx']['r'].predict(droneInputs) + self.r_dot_dr.predict(droneInputs)).__array__()[0][0]

		self.DfDx = np.matrix([
			[A11, A12, A13],
			[A21, A22, A23],
			[A31, A32, A33]
			])

		# DfDu
		# p_ddot eqs 
		B11 = (a11 * Mx['dfdu'][self.u[0]].predict(droneInputs)).__array__()[0][0]
		B12 = (a12 * My['dfdu'][self.u[1]].predict(droneInputs)).__array__()[0][0]
		B13 = (a13 * Mz['dfdu'][self.u[2]].predict(droneInputs)).__array__()[0][0]

		# q_ddot eqs 
		B21 = (a21 * Mx['dfdu'][self.u[0]].predict(droneInputs)).__array__()[0][0]
		B22 = (a22 * My['dfdu'][self.u[1]].predict(droneInputs)).__array__()[0][0]
		B23 = (a23 * Mz['dfdu'][self.u[2]].predict(droneInputs)).__array__()[0][0]

		# r_ddot eqs 
		B31 = (a31 * Mx['dfdu'][self.u[0]].predict(droneInputs)).__array__()[0][0]
		B32 = (a32 * My['dfdu'][self.u[1]].predict(droneInputs)).__array__()[0][0]
		B33 = (a33 * Mz['dfdu'][self.u[2]].predict(droneInputs)).__array__()[0][0]

		self.DfDu = np.matrix([
			[B11, B12, B13],
			[B21, B22, B23],
			[B31, B32, B33]
			])

		return self.DfDx, self.DfDu
		

	def predict(self, droneInputs, x_dot, u_dot):
		A, B = self.getAB(droneInputs)
		return A*x_dot + B*u_dot