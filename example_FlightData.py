'''
Example using Feasibly Controllable Metric (FCM) to label flight data. 
'''
# Imports
import os
import numpy as np

from processing import quadrotor, utility
from core import FCM


# Global parameters
SHOW_INFO = True # Print info statements
FCM_CF = 30 # FCM cutoff frequency
FCM_MVW = 500 # FCM cutoff frequency
FCM_BCF = 30 # Control effectiveness estimation low-pass cutoff frequency 
Parallelize = False # Whether to parallelize FCM computation across flights 
LOC_TYPE = 'Saturation-Yaw'


# Load flight data
dataDir = 'data'
droneConfigFile = 'CineGo-NDConfig.json'
logFile = 'CineGO_file_log.csv'
rows = ['2-15'] # Rows in logFile for which flight data should be imported

# Find and load quadrotor flight through the experiment logFile
fileLogPath = os.path.join(dataDir, logFile)
fileLog = quadrotor.loadFileLog(fileLogPath)
rows = utility._parseRows(rows)

droneConfigPath = os.path.join(dataDir, droneConfigFile)

if SHOW_INFO:
    print(f'[ INFO ] Loading data from: {logFile}')
    print('\t' + f'Directory: {dataDir}')
    print('\t' + f'Drone Config: {droneConfigFile}')

'''
LOAD FLIGHT DATA
'''
droneParams = quadrotor.loadDroneParams(droneConfigPath)
flightData = quadrotor.importData(fileLogPath, rows, droneParams, showINFO=SHOW_INFO)

if SHOW_INFO:
    print('[ INFO ] Estimating actuator rate constant')
# Derive rotor motor rate constants for each flight
taus = np.zeros((len([d['Data'] for d in flightData]), 4))
for i, d in enumerate([d['Data'] for d in flightData]):
    _taus = quadrotor.estimateActuatorDynamics(d, threshold = (2*np.pi/60) * 50)
    taus[i] = np.array([v for v in _taus.values()])

# Take median estimates and add to droneParams
tau = np.nanmedian(taus, axis = 0)
tausDict = {'taus':{k:v for k, v in zip(['w1', 'w2', 'w3', 'w4'], list(tau))}}
droneParams.update(tausDict)


# Compute FCM
LOCResults = FCM.findLOC_FCM_offline(flightData, 
                                     LOCCriteria='Saturation-Yaw', 
                                     estimateB = True, 
                                     estimateB_LPFCutoff=FCM_BCF,
                                     window=FCM_MVW,
                                     cutoff=FCM_CF,
                                     parallel=Parallelize)

# Show results
print(LOCResults[['Flight ID', 'LOC Type', 'Time LOC (start)', 'Time LOC (end)']])



# Plot results
from matplotlib import pyplot as plt
from matplotlib.ticker import AutoMinorLocator
import matplotlib.patches as mpatches

def prettifyAxis(ax):
    ax.xaxis.set_minor_locator(AutoMinorLocator())
    ax.yaxis.set_minor_locator(AutoMinorLocator())
    ax.tick_params(which='both', direction='in')

def addXVSPAN(ax, xmin, xmax, **kwargs):
    xlim = ax.get_xlim()
    ax.axvspan(xmin, xmax, **kwargs)
    ax.set_xlim(xlim)

def addLegendPatch(handles, **patchKwargs):
    handles.append(mpatches.Patch(**patchKwargs))


# Show example plot
FID = 64
_data = [f for f in flightData if f['Flight ID'] == FID][0]
_locResults = LOCResults[LOCResults['Flight ID'] == FID]
idxFCM = np.where(_data['Data']['t'] >= _locResults['Time LOC (end)'].to_numpy()[0])[0][0]


fig = plt.figure()
ax1 = fig.add_subplot(211)
ax2 = fig.add_subplot(212, sharex=ax1)
ax1.plot(_data['Data']['t'], _data['Data']['roll'], color = 'dimgrey', alpha = 0.4, linewidth = 1)
addXVSPAN(ax1, _data['Data']['t'][idxFCM - FCM_MVW], _data['Data']['t'][idxFCM], color = 'orange', alpha = 0.3, label = 'FCM Window')
ax1.plot(_data['Data']['t'][:idxFCM], _data['Data']['roll'][:idxFCM], color = 'k', linewidth = 2)
ax1.scatter(_data['Data']['t'][idxFCM], _data['Data']['roll'][idxFCM], s = 75, color = 'k', label = 'Current sample', zorder = 10)
ax2.plot(_data['Data']['t'], _data['Data']['pitch'], color = 'dimgrey', alpha = 0.4, linewidth = 1)
addXVSPAN(ax2, _data['Data']['t'][idxFCM - FCM_MVW], _data['Data']['t'][idxFCM], color = 'orange', alpha = 0.3, label = 'FCM Window')
ax2.plot(_data['Data']['t'][:idxFCM], _data['Data']['pitch'][:idxFCM], color = 'k', linewidth = 2)
ax2.scatter(_data['Data']['t'][idxFCM], _data['Data']['pitch'][idxFCM], s = 75, color = 'k', label = 'Current sample', zorder = 10)
ax1.legend()
ax1.set_ylabel('Roll, rad', fontsize = 14)
ax2.set_ylabel('Pitch, rad', fontsize = 14)
ax2.set_xlabel('Time, s', fontsize = 14)
prettifyAxis(ax1)
prettifyAxis(ax2)
plt.tight_layout()

plt.show()
