# Feasibly controllable metric (FCM) for loss-of-control (LOC) detection
This repository is associated with the publication: ["A novel metric for detecting quadrotor loss-of-control"](https://doi.org/10.1109/ICRA57147.2024.10610662) presented at the IEEE International Conference on Robotics and Automation (ICRA) 2024 in Yokohama, Japan. 

## Requirements
This repository has the following requirements
- Python 3.8+
- matplotlib (3.7.2)
- numpy (1.24.4)
- pandas (2.0.3)
- scipy (1.10.1)
- tqdm (4.66.5)


## Examples
'example_FlightData.py' demonstrates how the FCM may be applied to exisiting flight data
'example_simulation.py' demonstrates how the FCM may be used to detect LOC in a quadrotor simulation

The main FCM logic can be found in core/FCM.py (note that, if you would like to use the FCM alone, then the only requirement on libraries is numpy; the other libraries in the *Requirements* above are to process the flight data and run the simulator.)


# Associated paper(s)
When using the code in this repository, please refer to the following paper which introduces the feasibly controllable metric:

1. J. J. Van Beers, P. Solanki and C. C. De Visser, "A novel metric for detecting quadrotor loss-of-control," 2024 IEEE International Conference on Robotics and Automation (ICRA), Yokohama, Japan, 2024, pp. 15570-15576, doi: [10.1109/ICRA57147.2024.10610662](https://doi.org/10.1109/ICRA57147.2024.10610662).

Moreover, since this paper makes direct use of prior work, so it is also appreciated if the following papers are also acknowledged where appropriate. 

If using the example flight data provided, please also cite the paper associated with the data collection itself:

2. Altena, A. V., van Beers, J. J., & de Visser, C. C. (2023). Loss-of-Control Prediction of a Quadcopter Using Recurrent Neural Networks. Journal of Aerospace Information Systems, 20(10), 648-659. doi: [10.2514/1.I011231](https://doi.org/10.2514/1.I011231) 

Finally, if using the accompanying simulation (including only the provided quadrotor model), please consider citing some of the following papers:

3. van Beers, J. J., & de Visser, C. C. (2023). Peaking into the black-box: prediction intervals give insight into data-driven quadrotor model reliability. In AIAA SCITECh 2023 forum (p. 0125). doi: [10.2514/6.2023-0125](https://doi.org/10.2514/6.2023-0125)

4. Sun, S., & de Visser, C. (2019). Aerodynamic model identification of a quadrotor subjected to rotor failures in the high-speed flight regime. IEEE Robotics and Automation Letters, 4(4), 3868-3875. doi: [10.1109/LRA.2019.2928758](https://doi.org/10.1109/LRA.2019.2928758)

4. Sun, S., de Visser, C. C., & Chu, Q. (2019). Quadrotor gray-box model identification from high-speed flight data. Journal of Aircraft, 56(2), 645-661. doi: [10.2514/1.C035135](https://doi.org/10.2514/1.C035135)