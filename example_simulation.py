from simulator.model import droneModel
from simulator.controller import INDI
from simulator.actuator import droneRotors
from simulator.funcs import droneEOM, integrators
from simulator.noise import droneSensorNoise
from simulator.viz import animate, drone
import simulator.sim as sim
from simulator.funcs import plotting
from core import FCM

import os
import numpy as np
import matplotlib.pyplot as plt


'''
SIMULATION PARAMETERS
'''
# Model directory and name
modelPath = os.path.join(os.getcwd(), 'simulator/model')
modelID = 'MDL-HDBeetle-NN-II-NGP012-Linear-Sym'

# Controller gains file
PIDGainPath = os.path.join(os.getcwd(), 'simulator/controller')
PIDGainFile = 'droneINDIGains.json'

# Simulation duration, in seconds
tMax = 10
# Time step, in seconds
dt = 0.004

# Configure if noise should be added or not
addNoise = True 
# Noise seed
noiseSeed = 123456

# Animation parameters
showAnimation = True    # If quadrotor visualization should be showed
saveAnimation = False   # If quadrotor visualization should be saved

# Rotor Faults
doFault = True          # Whether rotor faults should be injected
# Modify actuation to simulate compelte rotor loss
rotorIndices = [3]      # Rotor indices with fault
rotorLimits = [0]       # LOE per rotor [0, 1] -> 1 is healthy 0 is complete rotor loss, 
                        #   corresponds to rotorIndices
# Time of failure
timeFailure = 5.2




'''
Functions
'''
# Rotor fault function
def modifiedActuator(simVars):
    if simVars['currentTimeStep_index'] >= int(timeFailure/simVars['dt']):
        for inum, rIdx in enumerate(rotorIndices):
            simulator.actuator.currentLims[rIdx]['lower'] = rotorLimits[inum]
            simulator.actuator.currentLims[rIdx]['upper'] = rotorLimits[inum]
    return simulator.actuator.actuate(simVars)



''' 
Model initialization

Load and initialize desired model using droneModel class.
'''
# Initialize the model
model = droneModel.Quadrotor(f'{modelPath}/{modelID}.json')
# Check if the models have been identified with gravity removed or not. If gravity has been removed, we should not remove it again in the EOM. 
if not model.hasGravity:
    model.droneParams['g'] = 0
originalMaxRPM = model.droneParams['max RPM']


'''
Equations of Motion

Initalize the equations of motion for the drone
'''
EOM = droneEOM.EOM


'''
Noise block configuration

Here we configure our (sensor) noise statistics, which will be fed to the controller
'''
# Define noise levels for each state
noiseMapping = {
    'roll':0.001,               # rad
    'pitch':0.001,              # rad
    'yaw':0.001,                # rad
    'u':0.01,                   # m/s
    'v':0.01,                   # m/s
    'w':0.01,                   # m/s
    'p':8.73e-05,               # rad/s (from MPU6000) 
    'q':8.73e-05,               # rad/s (from MPU6000)
    'r':8.73e-05,               # rad/s (from MPU6000)
    'x':0,
    'y':0,
    'z':0
}
noiseBlock = droneSensorNoise.droneNoiseBlock(globalSeed=noiseSeed)
if not addNoise:
    # Set noise scale to zero for no noise
    noiseBlock.mapStateNoiseScales(np.arange(0, 12, 1), np.zeros(12))
else:
    for k, v in noiseMapping.items():
        noiseBlock._setStateNoiseScaling(k, v)


'''
Controller initialization

Load controller (incl. subcontrollers, planners, etc.) by calling relevant class.

Here we use the provided PID controller for simplicity. Other controller architectures may also be designed.
'''
# Get mapping of inputs to direction of change in quadrotor body axes from model
signMask = model.getSignMask()
# In case there is an axis mismatch, specify it here.
# By default, we use a right-handed system where x points forwards (thumb finger), y to the left (index finger), and z down (middle finger)                    
signSwitch = np.array([1, 1, 1]) # Needed to match axis definition to drone configuration
RatePIDSignMask = signMask * signSwitch

# Initialize controller with control moment mapping and gains
controller = INDI.controller(RatePIDSignMask, noiseBlock, PIDGainPath, PIDGainFile=PIDGainFile)


'''
Actuator dynamics 

Define rotor actuator dynamics and initialize the actuator (droneRotors) class
'''
# Parameterize Rotor dynamics
if 'taus' in model.droneParams.keys():
    timeConstant = [v for v in model.droneParams['taus'].values()]
    print('[ INFO ] Found model-specific actuator time constant. Using these over default.')
else:
    timeConstant = [1/35,]*4

# Initialize actuator object
actuator = droneRotors.rotorDynamicsV2(timeConstant, droneParameters=model.droneParams, w0 = np.zeros((4, 1)))






'''
SIMULATION INITIALIZATION

Define simulation, reference signals, and run simulation
'''
# Time array
time = np.arange(0, tMax, dt)

# Define state vector: [roll, pitch, yaw, u, v, w, p, q, r]
# - roll    = Roll angle of the quadrotor, in radians
# - pitch   = Pitch angle of the quadrotor, in radians
# - yaw     = Yaw angle of the quadrotor, in radians
# - u       = body velocity along x, in meters per second
# - v       = body velocity along y, in meters per second
# - w       = body velocity along z, in meters per second
# - p       = Roll rate about the x axis, in radians per second
# - q       = Pitch rate about the y axis, in radians per second
# - r       = yaw rate about the z axis, in radians per second
# - x       = position in x, in meters
# - y       = position in y, in meters
# - z       = position in z, in meters
state = np.zeros((len(time), 1, 12))

# Define inputs: [w1, w2, w3, w4]
# - wi      = Rotor speed of the ith rotor, in eRPM
rotorSpeeds = np.zeros((len(time), 1, 4)) 

# Define initial conditions
state[0, :, :] = 0


# Initialize reference signal.
reference = state.copy() 

# Define simple position tracking task
# Position in x, m
reference[int(4/dt):, 0, 9] = 4
# Position in y, m
reference[int(4/dt):, 0, 10] = -2
# Position in z, m (+ve downwards!)
reference[int(2/dt):, 0, 11] = -2


# Define initial rotor speeds, set to idle
rotorSpeeds[0, :, :] = float(model.droneParams['wHover (eRPM)'])
# Set initial actuator rotor speed to initial rotor speed
actuator.setInitial(rotorSpeeds[0, :, :])



# Define animator for visualization
droneViz = drone.body(model, origin=state[0, 0, 9:12], rpy=state[0, 0, :3])
animator = animate.animation()
animator.addActor(droneViz, modelID)

# Pass on all simulation objects to runSim.sim() to define the simulator.
simulator = sim.sim(model, EOM, controller, actuator, animator=animator, showAnimation=showAnimation)

# Use custom integrator
simulator.assignIntegrator(integrators.droneIntegrator_Euler) # Faster
# simulator.assignIntegrator(integrators.droneIntegrator_rk4) # Two options; one uses model and another does not. See integrators.py

# Set initial state of simulator
simulator.setInitial(time, state, rotorSpeeds, reference)


'''
Rotor failures
'''
hasFailure = doFault
if doFault:
    simulator.doActuation = modifiedActuator

if timeFailure > time[-1]:
    print(f'[ WARNING ] timeFailure ({timeFailure}) exceeds simulation time ({time[-1]})')


'''
MAIN SIMULATION LOOP
'''
# Print key variables:
print('[ INFO ] SIMULATION PARAMETERS')
print('{:^8}'.format('') + f'Controller: \t\t\t{controller.name}')
print('{:^8}'.format('') + f'Add noise: \t\t\t{addNoise}')
print('{:^8}'.format('') + f'Drone model: \t\t\t{model.modelID}')
print('{:^8}'.format('') + f'Actuators: \t\t\t{actuator.name}')

# Run simulation
simulator.run()




# Save results
saveDir = 'simResults'
savePath = os.path.join(os.getcwd(), saveDir)
if not os.path.exists(saveDir):
    os.makedirs(savePath)


# Save animation
if saveAnimation:
    print('[ INFO ] Saving animation...')
    simulator.simVars['aniSaver'](os.path.join(savePath, 'animated_slowed.mp4'), dpi = 350, fps = 30)



print('[ INFO ] Creating and saving plots...')
# Show states and inputs
plotting.plotResults(simulator, savePath)





# FCM results 
MVW = controller.estimationWindow

B = np.array(controller.Bmat)
G = np.array(controller.Gmat)

fig = plt.figure(figsize = (12, 6))
gs = fig.add_gridspec(4, 5)
ax1 = fig.add_subplot(gs[:2, :3])
ax4 = fig.add_subplot(gs[2:, :3], sharex = ax1)
ax3 = fig.add_subplot(gs[:, 3:], projection = '3d')

objectsPoses = {
    simulator.model.modelID:{
        'time':simulator.simVars['time'],
        'position':simulator.simVars['state'][:, :, 9:12],
        'rotation_q':simulator.simVars['quat'],
        'inputs':simulator.simVars['inputs']
    }
}
animator.posHorizon = int(3.4/dt)
tArray = [
    simulator.simVars['time'][int(0.3*len(simulator.simVars['time']))],
    simulator.simVars['time'][int(0.5*len(simulator.simVars['time']))],
    simulator.simVars['time'][int(0.75*len(simulator.simVars['time']))],
    simulator.simVars['time'][int(0.8*len(simulator.simVars['time']))],
    simulator.simVars['time'][int(0.9*len(simulator.simVars['time']))],
    simulator.simVars['time'][int(0.95*len(simulator.simVars['time']))]
    ]
animator.asImage(objectsPoses, tArray, parentFig=fig)

colors = ['royalblue', 'mediumvioletred', 'mediumorchid']
labels = ['Up', 'Uq', 'Ur']
for i, cl in enumerate(colors):
    ax1.plot(simulator.simVars['time'][-len(B):], B[:, i, i]*G[:, i, i], color = cl, label = labels[i])

ax1.legend()
ax4.plot(simulator.simVars['time'][-len(controller.isStable):], controller.isStable, color = 'royalblue', alpha = 0.5, linewidth = 1.5, label = 'Instantaneous FCM')
triggers = np.array(controller.isStable)
MVW_isStable = FCM.iFCM2W(MVW, triggers)/2
ax4.plot(simulator.simVars['time'][-len(MVW_isStable):], MVW_isStable.astype(int), color = 'royalblue', linewidth = 2.5, label = f'Windowed FCM (W = {MVW*dt} s)')
ax4.set_yticks([0, 1])
ax4.set_yticklabels(['False', 'True'])
ax4.legend()
ax4.set_xlabel(r'$\mathbf{Time}$, s', fontsize = 12)
ax4.set_ylabel(r'Feasibly Controllable, -', fontsize = 12)
ax1.set_ylabel(r'Control Effectiveness, -', fontsize = 12)

if hasFailure:
    plotting.addVLINE(ax1, timeFailure, -100, 100, linestyle = '--', color = 'firebrick')
    plotting.addVLINE(ax4, timeFailure, -1, 2, linestyle = '--', color = 'firebrick')

plotting.prettifyAxis(ax1)
plotting.prettifyAxis(ax4)
plt.setp(ax1.get_xticklabels(), visible = False)
plt.subplots_adjust(left = 0.08, bottom = 0.08, top = 0.92)

fig.savefig('simResults/Controllability.pdf')
fig.savefig('simResults/Controllability.png', dpi = 600)




fig = plt.figure(figsize = (12, 6))
gs = fig.add_gridspec(3, 5)
ax1 = fig.add_subplot(gs[0, :3])
ax2 = fig.add_subplot(gs[1, :3], sharex = ax1)
ax4 = fig.add_subplot(gs[2, :3], sharex = ax1)
ax3 = fig.add_subplot(gs[:, 3:], projection = '3d')

animator.asImage(objectsPoses, tArray, parentFig=fig)
for i, cl in enumerate(colors):
    ax1.plot(simulator.simVars['time'][-len(B):], B[:, i, i], color = cl)
    ax2.plot(simulator.simVars['time'][-len(B):], G[:, i, i], color = cl)
    ax4.plot(simulator.simVars['time'][-len(B):], B[:, i, i]*G[:, i, i], color = cl)

ax4.set_xlabel(r'$\mathbf{Time}$, s', fontsize = 16)
ax4.set_ylabel(r'$\tilde{B}_{k}$', fontsize = 16)
ax2.set_ylabel(r'$M_{k}$', fontsize = 16)
ax1.set_ylabel(r'$\hat{B}_{k}$', fontsize = 16)
handles = []
plotting.addLegendLine(handles, color = colors[0], label = r'$U_{p}$')
plotting.addLegendLine(handles, color = colors[1], label = r'$U_{q}$')
plotting.addLegendLine(handles, color = colors[2], label = r'$U_{r}$')
ax1.legend(handles = handles)

if hasFailure:
    plotting.addVLINE(ax1, timeFailure, -100, 100, linestyle = '--', color = 'firebrick')
    plotting.addVLINE(ax2, timeFailure, -2, 2, linestyle = '--', color = 'firebrick')
    plotting.addVLINE(ax4, timeFailure, -100, 100, linestyle = '--', color = 'firebrick')

plotting.prettifyAxis(ax1)
plotting.prettifyAxis(ax2)
plotting.prettifyAxis(ax4)
plt.setp(ax1.get_xticklabels(), visible = False)
plt.setp(ax2.get_xticklabels(), visible = False)
plt.subplots_adjust(left = 0.08, bottom = 0.08, top = 0.92)

fig.savefig('simResults/ControlEffectiveness.pdf')
fig.savefig('simResults/ControlEffectiveness.png', dpi = 600)

plt.show()


# Show coefficients of the velocity form, over the flight duration
Ak = np.array(controller.Amat)
Bk = np.array(controller.Bmat)
fig1 = plt.figure(figsize = (10, 6))
gs1 = fig1.add_gridspec(3, 3)
fig2 = plt.figure(figsize = (10, 6))
gs2 = fig1.add_gridspec(3, 3)
for i in range(3):
    for o in range(3):
        ax1 = fig1.add_subplot(gs1[i, o])
        ax1.plot(np.array(Ak)[:, i, o], label = r'$A_{k}$' + f'[{i+1}, {o+1}]', linewidth = 2)
        ax2 = fig2.add_subplot(gs2[i, o])
        ax2.plot(np.array(Bk)[:, i, o], label = r'$B_{k}$' + f'[{i+1}, {o+1}]', linewidth = 2)
        ax1.legend()
        ax2.legend()
        ax1.set_ylim([np.min(Ak[:, i, :])-0.02, np.max(Ak[:, i, :])+0.1*np.abs(np.max(Ak[:, i, :]))])
        ax2.set_ylim([np.min(Bk[:, i, :])-0.00001, np.max(Bk[:, i, :])+0.1*np.abs(np.max(Bk[:, i, :]))])
        plotting.prettifyAxis(ax1)
        plotting.prettifyAxis(ax2)
        if i != 2:
            plt.setp(ax1.get_xticklabels(), visible = False)
            plt.setp(ax2.get_xticklabels(), visible = False)
        if o != 0:
            plt.setp(ax1.get_yticklabels(), visible = False)
            plt.setp(ax2.get_yticklabels(), visible = False)            
        if i == 1 and o == 0:
            ax1.set_ylabel(r'$\mathbf{Coefficient}, -$', fontsize = 14)
            ax2.set_ylabel(r'$\mathbf{Coefficient}, -$', fontsize = 14)
        if i == 2 and o == 1:
            ax2.set_xlabel(r'$\mathbf{Sample}, -$', fontsize = 14)
            ax1.set_xlabel(r'$\mathbf{Sample}, -$', fontsize = 14)
        fig1.tight_layout()
        fig2.tight_layout()



fig1.savefig('simResults/Ak_Coefficients.png', dpi = 600)
fig2.savefig('simResults/Bk_Coefficients.png', dpi = 600)
plt.show()