'''
Main Feasibly Controllable Metric (FCM) computation functions

Copyright: Jasper van Beers (2024-05-17)
Writen by: Jasper van Beers (contact: j.j.vanbeers@tudelft.nl)
'''
# Imports
import numpy as np
import pandas as pd
import concurrent.futures
import os
from tqdm import tqdm
from processing import utility


# Main FCM logic
def _iFCM(DfDx, DfDu, uk, duk, dxk, dxdotk, dt, droneParams, model_correction = 0):
    """
    Computes the instantenous feasibly controllable metric (iFCM) using a first order rotor actuator model
    
    Parameters
    ----------
    DfDx : numpy.matrix 
        `A` in `dx_dot = Adx + Bdu` (i.e. partial derivative of x_dot w.r.t x)
    DfDu : numpy.matrix 
        `B` in `dx_dot = Adx + Bdu` (i.e. partial derivative of x_dot w.r.t u)
    uk : numpy.array of 
        input vector at step k of shape (N,) or (1 x N)
    duk : numpy.array of 
        change in input vector between step k and k+1 of shape (N,) or (1 x N)
    dxk : numpy.array 
        change in state vector between step k and k+1 of shape (N,) or (1 x N)
    dxdotk : numpy.array 
        change in state derivative vector between step k and k+1 of shape (N,) or (1 x N)
    dt : float 
        time step between step k and k+1
    droneParams : dict 
        Relevant drone parameters. 
            `droneParams` must have keys:
            -`max RPM` - The upper saturation bound of the rotors
            -`min RPM` - The lower saturation bound of the rotors
            -`rotor config` - Mapping of the rotor index to position on the quadrotor, e.g.:
                       1    3     ^X        
                        \  /      |           
                         \/       |____> Y 
                         /\       (Z down)
                        /  \ 
                       2    4
                    = {'rotor config':{'front left':1, 'front right':3, 'aft left':2, 'aft right':4}}
            -`rotor1 rotation direction` - Either "CW" (clockwise) or "CCW" (counter-clockwise) as viewed from the top    
    model_correction : float 
        Additional correction factor for static dynamics (i.e. dx_dot = Adx + Bdu - model_correction), default is 0. 


    Returns
    -------
    boolean 
        Whether system is feasibly controllable at step k
    """
    # In order to determine delta_u_max (i.e. maximum counteractive control action), we need to know the rotor layout
    rotorMapping = _getSignMask(droneParams)
    # Assume controllable
    iFCM = True
    # Get current samples, and reshape for matrix manipulation
    _du = duk.reshape(1, -1).T
    _diff_rate = dxk.reshape(1, -1).T
    _diff_ratedot = dxdotk.reshape(1, -1).T
    # Get error between model and measurements
    #   model_correction used to account for additional effects (known but unmodelled dynamics, disturbances etc.)
    error = _diff_ratedot - np.matmul(DfDx, _diff_rate) + model_correction
    Buk = np.matmul(DfDu, _du)
    # Define saturation bounds (accounts for updates if droneParams is updated)
    refOmega = {-1:droneParams['max RPM'], 1:droneParams['idle RPM']}
    # Define necessary stabilizing control effort
    deltaUStab = np.matmul(np.linalg.inv(DfDu), error - Buk)
    # Check if deltaUstab is feasible, for each axis
    #   if ANY axis is infeasible then assume deltaUstab infeasible for all
    for i in range(error.shape[0]):
        # Get error remainder
        e = error.__array__()[i] - Buk.__array__()[i]
        # If error is near zero, set deltaUMax > deltaUStab
        if np.isclose(e, 0):
            deltaUMax = np.abs(deltaUStab.__array__()[i]) + 1
        else:
            # Assume worst
            deltaUMax = 0
            # Get necessary direction of change (opposing error)
            signMask = np.sign(e)*rotorMapping.T[i, :]
            # Get maximum change, per rotor
            for j, w in enumerate(uk):
                # Compare current rotor speed with appropriate saturation bound
                maxDiff = np.abs(w - refOmega[signMask[j]])
                # Get deltaUMax, in rad/s, based on first order drone model
                deltaUMax += 2*np.pi/60*(dt*(1/list(droneParams['taus'].values())[j])*(maxDiff))
        # Check feasibly controllable condition
        if np.abs(deltaUStab.__array__()[i]) > deltaUMax:
            iFCM = False
    return iFCM



def iFCM2W(W, iFCM):
    '''
    Computes the windowed feasibly controllable metric (FCMW)
    
    Parameters
    ----------
    W : int 
        window size, in samples
    iFCM : list-like
        instantenous FCM results. NOTE: Requires len(iFCM) >= W. 

    Returns
    -------
    numpy.array 
        of [2, 1, 0] indicating if the system is feasibly controllable (2 = Controllable, 1 = Transition between controllable and not, 0 = Not controllable)
    '''
    if len(iFCM) < W:
        raise ValueError(f'ERROR @ iFCM2W: window size ({W}) must be smaller than the iFCM list (length = {len(iFCM)})')
    isStableList = utility.majorityVoting(iFCM, W)
    isStableList = np.array([2,]*(W-1) + list(isStableList))
    return isStableList



# Label LOC moments using the Feasibly Controllable Metric
def findLOC_FCM_offline(flightData, LOCCriteria = 'Saturation-Yaw', estimateB = True, estimateB_LPFCutoff = 10, window = 300, cutoff = 30, parallel = True):
    '''Apply the feasibly controllable metric (FCM) to label moments of loss-of-control (LOC) in a collection of flight data

    INPUTS:
    :param flightData: LIST of dicts of the logged flight data
    :param LOCCriteria: STRING of the LOC type 
    :param estimateB: BOOLEAN indicating if the control effectiveness matrix, B (in x_dot = Ax + Bu) should be estimated
    :param estimateB_LPFCutoff: INT of the (low-pass) cutoff frequency, in Hz, used in the estimation of the control effectiveness matrix, B (if estimateB=True)
    :param window: INT Majority Window Voting size, in samples, used to compute FCM
    :param cutoff: INT (low-pass) filter cutoff frequency, in Hz, used to compute FCM
    :param parallel: BOOLEAN Whether LOC labelling should be parallelized or not

    OUTPUT:
    :param LOCData: pandas.DataFrame of the LOC labelling results
    '''
    # Define LOC label results columns to be filled
    columns = {
        'Flight ID':str,
        'Quadrotor':str,
        'LOC Algorithm':str,
        'Time LOC (start)':float,
        'Time LOC (end)':float,
        'LOC Type':str,
        'estimateB':bool,
        'B (LPF) cutoff frequency':int,
        'cutoff frequency':int,
        'majority voting window':int
    }
    # Estimate control effectivenes from flight data
    if estimateB:
        Bs = np.zeros((len(flightData), 3))
        for j, d in enumerate([i['Data'] for i in flightData]):  
            Bs[j, :] = _FCM_findB(d, cutoff=estimateB_LPFCutoff)
        Bvec = np.nanmean(Bs, axis = 0)
    else:
        Bvec = None
    
    # Prepare LOC labelling
    rows = [d['Row'] for d in flightData]
    num_cores = int(os.cpu_count() - 2)
    locResults = {}
    if parallel:
        dataset = zip(flightData, rows, [window,]*len(rows), [Bvec,]*len(rows), [cutoff,]*len(rows))
        with concurrent.futures.ProcessPoolExecutor(max_workers=num_cores) as executor:
            futures = {executor.submit(_findLOC_FCM_offline, *data): data for data in dataset}
            for future in tqdm(concurrent.futures.as_completed(futures), total = len(rows), desc='Labelling FCM (Parallel)'):
                run_data = futures[future]
                try:
                    locResults[run_data[1]] = future.result()
                except Exception as exc:
                    print(f'{run_data[1]} failed to complete [Exception: {exc}]')
    else:
        for d, r in tqdm(zip(flightData, rows), total = len(rows), desc = 'Labelling FCM'):
            locResults.update({r:_findLOC_FCM_offline(d, r, window, Bvec, cutoff)})


    # Define (empty) dataframe to store LOC label results
    LOCData = pd.DataFrame(columns = columns.keys())
    for d in flightData:
        t = d['Data']['t'].to_numpy()
        # Find "start" of dangerous manoeuvre (e.g. yaw induced LOC)
        idxStart = _find_LOC_start(d['Data'], LOCCriteria)
        if len(idxStart):
            tStart = t[idxStart[0]]
        else:
            tStart = np.nan
        locRes = locResults[d['Row']]
        dSeries = pd.Series(index = columns.keys(), dtype = object)
        dSeries['Flight ID'] = d['Flight ID']
        dSeries['Quadrotor'] = d['Quadrotor']
        dSeries['LOC Algorithm'] = 'findLOC_FCM'
        dSeries['Time LOC (start)'] = tStart
        dSeries['Time LOC (end)'] = locRes['predLOC']
        dSeries['LOC Type'] = d['LOC Type']
        dSeries['estimateB'] = estimateB
        dSeries['B (LPF) cutoff frequency'] = estimateB_LPFCutoff
        dSeries['cutoff frequency'] = cutoff
        dSeries['majority voting window'] = window
        LOCData = pd.concat([LOCData, dSeries.to_frame().T], ignore_index=True)
    return LOCData



def _findLOC_FCM_offline(data, r, w, Bvec, cutoff):
    '''(Internal function to) Apply the feasibly controllable metric (FCM) to label moments of loss-of-control (LOC) in a single flight
    
    INPUTS:
    :param data: dict of the flight data with (at least) the following entries
                -'droneParams' dict of the drone parameters
                -'Data' pandas.DataFrame of the flight data logs
    :param r: int of the corresponding ID of the flight data
    :param Bvec: numpy.array of the estimated (diagonal) control effectiveness
    :param cutoff: int of the low-pass filter cutoff frequency to apply to the relevant flight data when computing the FCM

    OUTPUTS:
    :param results: dict of the FCM label results
    '''
    droneParams = data['droneParams']
    d = data['Data']
    N = len(d)
    d = d.loc[int(0.025*N):int(0.975*N), :].copy()
    B = np.diag(Bvec)
    iFCM = _findLOC_iFCM_offline(d, np.zeros((3, 3)), B, droneParams, cutoff = cutoff)
    isStableList = utility.majorityVoting(iFCM, w)
    isStableList = np.array([2,]*(w-1) + list(isStableList))
    if len(np.where(isStableList==0)[0]):
        predLOC = d['t'].to_numpy()[np.where(isStableList==0)[0][0]+1]
    else:
        predLOC = np.nan
    results = {}
    results.update({'_row':r})
    results.update({'predLOC':predLOC})
    results.update({'isStable':np.array(isStableList)})
    results.update({'isStableTriggers':np.array(iFCM)})
    return results



def _findLOC_iFCM_offline(data, A, B, droneParams, cutoff = 30):
    '''Function to estimate the (instantenous) feasibly controllable metric (FCM) on flight data
    
    :param data: pandas.DataFrame of flight data
    :param A: 3x3 numpy.array object of A in dx_dot = Adx + Bdu
    :param B: 3x3 numpy.array object of B in dx_dot = Adx + Bdu 
    :param droneParams: DICT of quadrotor parameters
    :param cutoff: INT (low-pass) cutoff frequency, in Hz, used to estimate FCM
    '''
    # Extract flight data columns
    t = data['t'].to_numpy()
    # Get representative time step
    dt = np.nanmedian(t[1:] - t[:-1])
    # Filter data using specified low-pass cutoff frequency 
    inputs = np.zeros((len(data), 4))
    for icol, col in enumerate(['w1', 'w2', 'w3', 'w4']):
        inputs[:, icol] = utility._ButterFilter(data[col].to_numpy(), int(1/dt), 4, cutoff, 'low')
    u = np.zeros((len(data), 3))
    for icol, col in enumerate(['u_p', 'u_q', 'u_r']):
        u[:, icol] = utility._ButterFilter(data[col].to_numpy()*2*np.pi/60, int(1/dt), 4, cutoff, 'low')
    # Get change in inputs, du
    du = u[1:] - u[:-1]
    rate = np.zeros((len(data), 3))
    for icol, col in enumerate(['p', 'q', 'r']):
        rate[:, icol] = utility._ButterFilter(data[col].to_numpy(), int(1/dt), 4, cutoff, 'low')
    ratedot = utility.derivative(rate, t)
    # Get change in state, dx
    diff_rate = rate[1:] - rate[:-1]
    # Get rate of change in state, dx_dot
    diff_ratedot = ratedot[1:] - ratedot[:-1]

    # Instantenous LOC 
    iFCM = []
    # Scan over flight
    for k in range(len(t)-1):
        # Get local dt (in case of dropped samples)
        dt = t[k+1] - t[k]
        isStableTriggers = _iFCM(A, B, inputs[k], du[k], diff_rate[k], diff_ratedot[k], dt, droneParams)
        iFCM.append(isStableTriggers)
    return iFCM



def _getSignMask(droneParams):
    '''Function to determine the mapping from inputs to (directional) influence on roll, pitch, and yaw rates
    
    INPUTS:
    :param droneParams: dict of the drone parameters with (at least) the following entries
                         -'rotor config' - Mapping of the rotor index to position on the quadrotor, e.g.:
                                  1    3     ^X        
                                   \  /      |           
                                    \/       |____> Y 
                                    /\       (Z down)
                                   /  \ 
                                  2    4
                                 = {'rotor config':{'front left':1, 'front right':3, 'aft left':2, 'aft right':4}}
                         -'rotor1 rotation direction' - Either "CW" (clockwise) or "CCW" (counter-clockwise) as viewed from the top

    OUTPUTS:
    :param signMask: numpy.array mapping a given rotor index to influence along roll, pitch, and yaw axes
    '''
    # Define dictionary which maps the rotor numbers to their position on the quadrotor (e.g. rotor 4 is located 'front left')
    invRotorConfig = {v:k for k, v in droneParams['rotor config'].items()}
    # Extract ditcionary which maps the yaw sign to CW (clockwise) and CCW (counterclockwise) rotation of the rotors
    r_sign = {'CCW':-1, 'CW':1}
    # Create a toggle to parse through rotor mapping using text cues (e.g. front, left, etc.)
    signMap = {True:1, False:-1}
    # Create a matrix mask describing the signs of each rotor on each of the control moments
    # -> A matrix which describes how the rotor configuration produces rotations of the quadrotor and the sign therein
    signMask = np.array([[signMap[invRotorConfig[1].endswith('left')], signMap[invRotorConfig[2].endswith('left')], signMap[invRotorConfig[3].endswith('left')], signMap[invRotorConfig[4].endswith('left')]],
                                [signMap[invRotorConfig[1].startswith('front')], signMap[invRotorConfig[2].startswith('front')], signMap[invRotorConfig[3].startswith('front')], signMap[invRotorConfig[4].startswith('front')]],
                                r_sign[droneParams['rotor1 rotation direction']]*np.array([1, -1, -1, 1])]).T
    return signMask



def _FCM_findB(d, trimRatio = 0.75, cutoff = 10):
    '''
    Function to estimate control effectiveness matrix, using dx_dot = B du (assumes Adx is negligible)

    INPUTS:
    :param d: pandas.DataFrame of the flight data 
    :param trimRatio: FLOAT between within the interval (0, 1) exclusive indicating how much data should be removed from start and end of the flight (trimRatio/2)
    :param cutoff: INT (low-pass) cut-off frequency used to smoothen flight data in control effectiveness 

    OUTPUTS:
    :param B: numpy.array of the estimated control effectiness matrix 
    '''
    N = len(d)
    # Trim data to avoid outliers
    forwardTrim = int(trimRatio/2)*N
    backwardTrim = N - forwardTrim
    data = d.loc[forwardTrim:backwardTrim, :]
    t = data['t'].to_numpy()
    # Get (representative) estimate of time step
    dt = np.nanmedian(t[1:] - t[:-1])
    # Pre-allocate B
    Bmat = np.zeros(3)
    for i, (xCol, uCol) in enumerate(zip(['p', 'q', 'r'], ['u_p', 'u_q', 'u_r'])):
        # Get smoothened estimates of u
        _u = utility._ButterFilter(data[uCol].to_numpy(), int(1/(dt)), 4, cutoff, 'low').reshape(-1, 1)
        # Get smoothened estimates of x (to calulcate dx_dot)
        __x = utility._ButterFilter(data[xCol].to_numpy(), int(1/(dt)), 4, cutoff, 'low').reshape(-1, 1)
        # Take derivative of x
        _x = utility.derivative(__x, t)
        # Get du
        du = np.matrix(_u[1:] - _u[:-1])
        # Get dx_dot
        dx = np.matrix(_x[1:] - _x[:-1])
        # Estimate control effectiveness, B, using OLS
        Bmat[i] = utility.OLS(dx, du).__array__()[0][0]
    return Bmat



def _find_LOC_start(data, LOCType):
    '''
    Utility function to find the starting index of a loss-of-control inducing manoeuvre from a set of known LOC scenarios    
    
    INPUTS: 
    :param data: pandas.DataFrame of the flight data
    :param LOCType: string of the LOC scenario to label

    OUTPUTS:
    :param : numpy.array of the manoeuvre start indices
    '''
    if LOCType not in LOC_START_MAPPER.keys():
        return np.array([])
    else:
        return LOC_START_MAPPER[LOCType](data)



def _find_LOC_start_yaw(data):
    '''
    Utility function to find the starting index of a loss-of-control event induced by the Yaw LOC manoeuvre

    INPUTS: 
    :param data: pandas.DataFrame of the flight data

    OUTPUTS:
    :param : numpy.array of the yaw manoeuvre start indices
    '''
    return np.where(np.abs(np.abs(data['yaw']) - np.abs(data['yaw'][0]))>=np.pi)[0]



'''
Default values
'''
LOC_START_MAPPER = {
    'Saturation-Yaw':_find_LOC_start_yaw
}
