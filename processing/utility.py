'''
Commonly used functions
'''
import numpy as np
import pandas as pd
from scipy.interpolate import interp1d
from scipy.signal import butter, filtfilt



def resample(Data, newFreq, interpolation_kind = 'linear'):
    time = Data['t'].to_numpy()
    vals = Data.to_numpy()
    func = interp1d(time, vals, kind = interpolation_kind, axis=0)
    newTime = np.arange(time[0], time[-1], 1/newFreq)
    newVals = func(newTime)
    resampled_Data = pd.DataFrame(newVals, columns=Data.columns)
    return resampled_Data


def _parseRows(rows):
    rowIdxs = []
    for r in rows:
        # Is number 
        if isinstance(r, int):
            rowIdxs.append(r)
        else:
            # Check if string integer
            try:
                _r = int(r)
                rowIdxs.append(_r)
            except ValueError:
                # Is a range
                rng = r.split('-')
                rRange = np.arange(int(rng[0]), int(rng[1])+1, dtype=int)
                for _r in rRange:
                    rowIdxs.append(_r)
    return rowIdxs


def aggregateData(dataList):
    i = 0
    # Create new indices for aggregated dataframe while keeping track of original indices
    for df in dataList:
        # Set new index values
        new_indices = np.arange(i, (i+len(df)))
        df.set_index(pd.Index(new_indices), inplace=True)
        # Remove automatically added column
        try:
            df.drop(labels=['Unnamed: 0'], axis = 1, inplace=True)
        except KeyError:
            pass
        i += len(df)
    aggrData = pd.concat(dataList)
    return aggrData


def _ButterFilter(y, fs, order, cutoff_freq, filt_type, analog=False):
    '''Utility function to apply a butterworth filter to input data
    
    :param y: Data for which filter should be applied
    :param fs: Sampling frequency, in Hz
    :param order: Order of butterworth filter, as int
    :param cutoff_freq: Cutoff frequency, in Hz
    :param filt_type: Boolean, indicating if filter is low-pass ('low') or high-pass ('high')
    :param analog: Boolean, indiciating if analog (True) or digital (False) filter should be returned. Default is False.

    :return: Pass filtered data
    '''
    b, a = butter(order, cutoff_freq, btype=filt_type, analog=analog, fs=fs, output='ba')
    filt_X = filtfilt(b, a, y)
    return filt_X


def derivative(x, t):
    '''Function to numerically obtain the derivative of x with respect to t
    
    :param x: Signal to differentiate
    :param t: Signal with which x should be differentiated by

    :return: dx/dt
    '''
    xdot = np.zeros(x.shape)
    for i in range(len(t)-2):
        xdot[i + 1, :] = 0.5 * (x[i + 2, :] - x[i, :])/(0.5 * (t[i+2] - t[i]))
        if np.isinf(xdot[i + 1, :]).any():
            # Correct for inf, if any
            idx_undef = np.where(np.isinf(xdot[i + 1, :]))[0]
            xdot[i + 1, idx_undef] = xdot[i-1, idx_undef]
    # Missing first and last point, set them equal to nearest point
    xdot[0, :] = xdot[1, :]
    xdot[-1, :] = xdot[-2, :]
    # Interpolate NaNs, needs to be done per index in xdot
    for i in range(xdot.shape[1]):
        nanLocs = np.isnan(xdot[:, i])
        nanIdxs = np.where(nanLocs)[0]
        if len(nanIdxs) > 0:
            # Create a function which linearly interpolates the signal, based on known data, which takes
            # index as input -> i.e. y = f(index) -< can be thought of as a proxy for time
            func = np.interp1d(np.where(~nanLocs)[0], xdot[~nanLocs, i], kind = 'slinear')
            # Interpolate NaN indexes 
            xdot[nanIdxs, i] = func(nanIdxs)
    return xdot



def majorityVoting(d, w):
    cumulative = np.cumsum((np.insert(d, 0, 0)))
    votes = cumulative[w:] - cumulative[:-w]
    averageVotes = votes/w
    assignedVotes = (2*averageVotes.copy()).round(0)
    return assignedVotes


def OLS(y, A):
    theta = np.linalg.inv(A.T*A)*A.T*y
    return theta


def scaleFromTo(x, fromMin, fromMax, toMin, toMax):
    return (x-fromMin)/(fromMax - fromMin) * (toMax - toMin) + toMin