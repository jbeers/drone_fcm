'''
Script to import data from csv
'''
import numpy as np
import pandas as pd
import os
import json

from processing import utility

def importData(filelogpath, rows, droneParams, forceCSV = False, CSVSaveDir = None, showINFO = True):
    # rows is a list of string range e.g. [i-j] or list of integers or both
    fileLog = loadFileLog(filelogpath)
    rowIdxs = utility._parseRows(rows)
    colMapping = {
                ' time (s)':'t',                    # s
                ' roll':'roll',                     # rad
                ' pitch':'pitch',                   # rad
                ' heading':'yaw',                   # rad (with offset)
                ' gyroADC[0] (rad/s)':'p',          # rad/s
                ' gyroADC[1] (rad/s)':'q',          # rad/s
                ' gyroADC[2] (rad/s)':'r',          # rad/s
                ' accSmooth[0] (m/s/s)':'ax',       # m/s2
                ' accSmooth[1] (m/s/s)':'ay',       # m/s2
                ' accSmooth[2] (m/s/s)':'az',       # m/s2
                ' debug[0]':'w1',                   # erpm
                ' debug[1]':'w2',                   # erpm
                ' debug[2]':'w3',                   # erpm
                ' debug[3]':'w4',                   # erpm
                ' vbatLatest (V)':'voltage',        # V
                ' amperageLatest (A)':'amperage',   # A
                ' motor[0]':'w1_CMD',               # - (Scaled output of betaflight controller)
                ' motor[1]':'w2_CMD',               # - (Scaled output of betaflight controller)
                ' motor[2]':'w3_CMD',               # - (Scaled output of betaflight controller)
                ' motor[3]':'w4_CMD'                # - (Scaled output of betaflight controller)
                }

    data = []
    for i, r in enumerate(rowIdxs):
        try:
            fileDir = fileLog.loc[r - 2, 'Onboard data directory']
            filename = fileLog.loc[r - 2, 'Onboard data filename']
        except KeyError:
            fileDir = fileLog.loc[r - 2, 'OB Path']
            filename = fileLog.loc[r - 2, 'Onboard Name']
        filenameCSV = filename.split('.')[0] + '.csv'
        if showINFO:
            print('[ INFO ] Loading {} from {} (row = {}) [{}/{}]'.format(filename, fileDir, r, i+1, len(rowIdxs)))
        if CSVSaveDir is not None:
            csvFileDir = CSVSaveDir
        else:
            csvFileDir = fileDir
        # Check if csv already exists
        if os.path.exists(os.path.join(csvFileDir, filenameCSV)) and not forceCSV:
            d = loadCSV(os.path.join(csvFileDir, filenameCSV), colMapping)
        else:
            raise ValueError(f'Could not find the file: {os.path.join(csvFileDir, filenameCSV)}')
        
        addControlMoments(d, droneParams)
        packedData = {}
        try:
            packedData.update({'Flight ID':fileLog.loc[r-2, 'Flight ID']})
        except KeyError:
            packedData.update({'Flight ID':fileLog.loc[r-2, 'ID']})
        packedData.update({'Quadrotor':fileLog.loc[r-2, 'Quadrotor']})
        try:
            packedData.update({'LOC Type':fileLog.loc[r-2, 'LOC Type']})
        except KeyError:
            packedData.update({'LOC Type':fileLog.loc[r-2, 'LOC type']})
        packedData.update({'Data':d})
        packedData.update({'Row':r})
        packedData.update({'droneParams':droneParams})
        data.append(packedData)

    return data


def loadFileLog(filelogpath):
    return pd.read_csv(filelogpath)


def loadCSV(filepath, columnMapping, delimiter = ',', header=0):
    # For some reason, the blackbox tool adds extra columns in the data fields, without column names (and delimiters)
    # As such, Pandas groups the data from these columns into the existing ones, causing a misalignment of the data and column. 
    # To circumvent this, we first read the headers only so we know how many columns we expect (which can vary across quadrotors)
    # We then instruct pandas to only read these N columns. 
    columns = pd.read_csv(filepath, delimiter=delimiter, header=header, nrows = 0, dtype = str).columns
    rawData = pd.read_csv(filepath, delimiter=delimiter, header=header, usecols=np.arange(0, len(columns)), dtype = object)

    # Some rows have the following
    #   S frame: 
    #   P Frame unusuable due to prior corruption
    # Need to remove these rows
    # -> Parse S frame removal by looking for first index and second index of time (s) = 0
    rawData.rename(columns=columnMapping, inplace=True)
    nanIdx = pd.to_numeric(rawData['loopIteration'], errors = 'coerce').isnull() # Coerce changes strings that are not numeric to nan
    # Drop nan columns
    rawData.drop(index = np.where(nanIdx)[0], inplace=True)
    Data = rawData.loc[:, list(columnMapping.values())].copy().reset_index().astype(float)
    Data['t'] = Data['t'] - Data.loc[0, 't']

    # Convert roll, pitch, yaw to rad and unrwap
    Data['roll'] = np.unwrap(Data['roll']*np.pi/180)
    Data['pitch'] = np.unwrap(Data['pitch']*np.pi/180)
    Data['yaw'] = np.unwrap(Data['yaw']*np.pi/180)*(-1)

    # Transform from betaflight axis system (x-forward, y-left, z-up) to conventional aerospace system (x-forward, y-right, z-down) 
    # Rotate 180 degrees about x axis
    flips = ['pitch', 'yaw', 'q', 'r', 'ay', 'az']
    for f in flips:
        Data[f] = -1*Data[f]
    return Data


def loadDroneParams(configFile):
    with open(configFile, 'r') as f:
        droneConfig = json.load(f)

    droneParams = {'R':float(droneConfig['rotor radius']),
                'b':float(droneConfig['b']),
                'Iv':np.array(np.matrix(droneConfig['moment of inertia'])),
                'idle RPM':float(droneConfig['idle RPM']),
                'max RPM':float(droneConfig['max RPM']),
                'rotor config':droneConfig['rotor config'],
                'idle RPM':float(droneConfig['idle RPM']),
                'max RPM':float(droneConfig['max RPM']),
                'mass':float(droneConfig['mass']),
                'rotor1 rotation direction':droneConfig['rotor1 rotation direction'], 
                'rho':1.225,
                'g':9.81}
    return droneParams


def addControlMoments(data, droneParams):
    up, uq, ur = _computeControlMoments(data[['w1', 'w2', 'w3', 'w4']].to_numpy(), droneParams)
    data['u_p'] = up
    data['u_q'] = uq
    data['u_r'] = ur
    up, uq, ur = _computeControlMoments(np.square(data[['w1', 'w2', 'w3', 'w4']].to_numpy()), droneParams)
    data['U_p'] = up
    data['U_q'] = uq
    data['U_r'] = ur
    up, uq, ur = _computeControlMoments(data[['w1_CMD', 'w2_CMD', 'w3_CMD', 'w4_CMD']].to_numpy(), droneParams)
    data['u_p_CMD'] = up
    data['u_q_CMD'] = uq
    data['u_r_CMD'] = ur
    up, uq, ur = _computeControlMoments(np.square(data[['w1_CMD', 'w2_CMD', 'w3_CMD', 'w4_CMD']].to_numpy()), droneParams)
    data['U_p_CMD'] = up
    data['U_q_CMD'] = uq
    data['U_r_CMD'] = ur
 

def _computeControlMoments(omega, droneParams):
    rotorConfig = droneParams['rotor config']
    r_sign = {'CCW':-1, 'CW':1}
    rotorDir = droneParams['rotor1 rotation direction']
    u_p = (omega[:, rotorConfig['front left'] - 1] + omega[:, rotorConfig['aft left'] - 1]) - (omega[:, rotorConfig['front right'] - 1] + omega[:, rotorConfig['aft right'] - 1])
    u_q = (omega[:, rotorConfig['front left'] - 1] + omega[:, rotorConfig['front right'] - 1]) - (omega[:, rotorConfig['aft left'] - 1] + omega[:, rotorConfig['aft right'] - 1])
    u_r = r_sign[rotorDir]*((omega[:, rotorConfig['front left'] - 1] + omega[:, rotorConfig['aft right']- 1]) - (omega[:, rotorConfig['front right'] - 1] + omega[:, rotorConfig['aft left'] - 1]))
    return u_p, u_q, u_r


def estimateActuatorDynamics(data, threshold = (2*np.pi/60) * 50):
    t = data['t'].to_numpy()
    dt = t[1] - t[0]
    fs = int(1/dt)
    rotors = ['w1', 'w2', 'w3', 'w4']
    taus = {}
    for r in rotors:
        idxs = np.where(np.abs(data[r] - data[r + '_CMD']) <= threshold)[0]
        w = utility._ButterFilter(data[r].to_numpy(), fs, 4, 10, 'low')
        wCMD = utility._ButterFilter(data[r + '_CMD'].to_numpy(), fs, 4, 10, 'low')
        w_dot = utility._ButterFilter(utility.derivative(data[r].to_numpy().reshape(-1, 1), t).reshape(-1), fs, 4, 10, 'low')
        try:
            wCMD = utility.scaleFromTo(wCMD, np.nanmin(wCMD), np.nanmax(wCMD), np.nanmin(w), np.nanmax(w))
            y = np.matrix(w_dot[idxs]).T
            x = np.matrix(wCMD[idxs] - w[idxs]).T
            theta = np.linalg.inv(x.T*x)*x.T*y
            tau = (1/theta).__array__()[0][0]
            if tau < 0:
                tau = np.nan
        except (np.linalg.LinAlgError, ValueError) as e:
            tau = np.nan
        taus.update({r:tau})
    return taus


def calcM(Iv, rates, t, derivativeMethod = None, derivativeKwargs = None):
    '''Function to calculate moments of an object using first principles
    
    :param Iv: [3x3] Matrix of the moment of inertias of the object
    :param rates: Array-like structure containing the samples of the object's rotational rate. Shape is Nx3, N = number of observations
    :param t: 1-D array of time associated with body rate measurements
    :param derivativeMethod: Function used to differentiate the rotational rates to obtain the rotational acceleration. Function must take the rates as the only positional argument. All other parameters must be specified as keyword values in derivativeKwargs. Default is finite difference (Default = None).
    :param derivativeKwargs: Keyword arguments necessary for the chosen differentiation function (i.e. derivativeMethod)

    :return: Moments associated with measured rates
    '''
    if derivativeMethod is None:
        derivativeMethod = utility._finiteDifference
        derivativeKwargs = {'t':t}
    d_rates = rates.copy()*0
    for r in range(rates.shape[1]):
        d_rates[:, r] = derivativeMethod(rates[:, r], **derivativeKwargs)
    I_alpha = np.matmul(Iv, d_rates.T)
    I_omega = np.matmul(Iv, rates.T)
    M_gyro = np.cross(rates, I_omega, axisa=-1, axisb=0)
    return np.array(I_alpha.T + M_gyro)


def addMoments(droneParams, filteredData):
    moments = calcM(droneParams['Iv'], filteredData[['p', 'q', 'r']].to_numpy(), t = filteredData['t'].to_numpy())
    filteredData['Mx'] = moments[:, 0]
    filteredData['My'] = moments[:, 1]
    filteredData['Mz'] = moments[:, 2] 